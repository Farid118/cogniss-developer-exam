const jsonfile = require("jsonfile");
const randomstring = require("randomstring");

const inputFile = "input2.json";
const outputFile = "output2.json";
var output = {}

console.log("loading input file", inputFile);
console.log('-=-=-=-=-=-=-=-=-=-\n')

jsonfile.readFile(inputFile, function(err, body){
    console.log("loaded input file content", body);
    console.log('-=-=-=-=-=-=-=-=-=-\n')

    output.emails = [];
    let {names} = body;

    console.log("Reverse the names, add 5 random characters and @gmail.com")

    names.forEach(item => {
        output.emails.push( item.split('').reverse().join('')+randomstring.generate(5)+"@gmail.com")
    })
    console.log("generated emails", output.emails);
    console.log('-=-=-=-=-=-=-=-=-=-\n')

    console.log("saving output file formatted with 2 space indenting");
    console.log('-=-=-=-=-=-=-=-=-=-\n')

    jsonfile.writeFile(outputFile, output, {spaces: 2}, function(err) {
        if (err) {
            console.log(err)
        }else{
            console.log("All done!");
        }
    });
})

