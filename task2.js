const jsonfile = require("jsonfile");
const randomstring = require("randomstring");
const got = require("got");

const inputURL = "http://www.nactem.ac.uk/software/acromine/dictionary.py?sf=";
const outputFile = "output3.json";

var output = {}
console.log("getting 10 acronyms\n");
output.definitions = [];
console.log("creating looping function\n")
const getAcronym = function() {
  var acronym = randomstring.generate(3).toUpperCase();
  got(inputURL+acronym).then(response => {
    console.log("got data for acronym", acronym);
    console.log("add returned data to definitions array\n");
    output.definitions.push(response.body);
    if (output.definitions.length < 10) {
      console.log("calling looping function again\n");
      getAcronym();
    }
  })
  .then(()=> {
    console.log("saving to the output file formatted with 2 space indenting\n");
    jsonfile.writeFile(outputFile, output, {spaces: 2}, function(err) {
      console.log("Done!\n");
    });
  })
  .catch(err => {
    console.log(err)
  })
}

console.log("calling looping function");
getAcronym();

